# Singularity Containers 

Learnt from [**here**](https://github.com/idia-astro/idia-container-casakernel)

## Download Singularity

These steps are successfully done in ubuntu 18.04

[**Procedure**](https://ska-telescope.gitlab.io/src/ska-src-training-containers/#h.cpchh14b5v93)

_or_ 

**Essential packages**

```
sudo apt-get update
sudo apt-get install -y build-essential ibssl-dev uuid-dev libgpgme11-dev squashfs-tools libseccomp-dev pkg-config
```

**For all flavours, download and install Go**

```
sudo snap install go --classic
```

**Download, compile and install Singularity**

```
wget https://github.com/sylabs/singularity/releases/download/v3.9.0/singularity-ce-3.9.0.tar.gz
tar -xzf singularity-ce-3.9.0.tar.gz
cd singularity-ce-3.9.0
./mconfig
make -C ./builddir
sudo make -C ./builddir install
```

**After that verify if Singularity Engine is installed correctly by running**

```
singularity --version
```

## How to compile singularity on your created def file

```
sudo singularity build filename.sif name-of-your-file.def

e.g.

sudo singularity build CASA_WSclean.sif container_casa-wsclean.def

```

## Running the environment on your computer

```
./filename.sif
```

## Explaination of files

**container_casa-wsclean.def** 

- [CASA 5.4]
- [wsclean 3.0]

**container_CWA.def**

- [CASA 5.4]
- [wsclean 3.0]
- [AOFlagger 3.1]

**radio_tools.def**

- [CASA 5.6]
- [wsclean 3.0]
- [AOFlagger 3.1]
- [Pybdsf 1.10.1]

```
#If you want to install astropy into casa follow this after opening casa in Singularity (you have to do it once)

import subprocess, sys
subprocess.check_call([sys.executable, '-m', 'pip', 'install'])
import pip
pip.main(['install', 'astropy', '--user'])

#exit from casa and open casa and try again for astropy

```

# While Running Singularity; if you find some issues

## like your data is stored in mounted drive /DATA ;

in this case you have to **bind** the drive with singularity

```
singularity shell --bind /path-on-your-drive-you-want-to-mount:/name-you-want-to-give-for-your-container /path/to/your/sif/file

e.g.

singularity shell --bind /DATA/mangla:/DATA /home/mangla/CASA_WSclean.sif
```















